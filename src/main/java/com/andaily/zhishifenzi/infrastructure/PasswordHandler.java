package com.andaily.zhishifenzi.infrastructure;

import com.andaily.zhishifenzi.domain.shared.GuidGenerator;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.util.StringUtils;

/**
 * @author Shengzhao Li
 */
public abstract class PasswordHandler {


    /**
     * Encrypt factor, use it for  reversible  password
     */
    private static final int ENCRYPT_FACTOR = 7;

    private static final int STRENGTH = 256;

    /**
     * Return a random password from {@link java.util.UUID},
     * the length is 10.
     *
     * @return Random password
     */
    public static String randomPassword() {
        String uuid = GuidGenerator.generate();
        return uuid.substring(0, 10);
    }

    /**
     * Encrypt  password ,if original password is empty,
     * will call {@link #randomPassword()}  get a random original password.
     *
     * @param originalPassword Original password
     * @return Encrypted password
     */
    public static String encryptPassword(String originalPassword, String salt) {
        ShaPasswordEncoder passwordEncoder = new ShaPasswordEncoder(STRENGTH);
        return passwordEncoder.encodePassword(originalPassword, salt);
    }

    /**
     * Encrypt the reversible password
     *
     * @param originalPassword originalPassword
     * @return encrypted password
     */
    public static String encryptReversiblePassword(String originalPassword) {
        if (!StringUtils.hasText(originalPassword)) {
            return originalPassword;
        }
        byte[] bytes = originalPassword.getBytes();
        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];
            bytes[i] = (byte) (b ^ ENCRYPT_FACTOR);
        }
        return new String(bytes);
    }

    /**
     * Decrypt the encrypted password.
     *
     * @param encryptedPassword encryptedPassword
     * @return decrypted password
     */
    public static String decryptReversiblePassword(String encryptedPassword) {
        return encryptReversiblePassword(encryptedPassword);
    }
}