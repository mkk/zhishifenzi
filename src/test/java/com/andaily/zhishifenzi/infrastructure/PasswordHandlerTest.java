package com.andaily.zhishifenzi.infrastructure;

import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

/**
 * @author Shengzhao Li
 */
public class PasswordHandlerTest {

    @Test
    public void testDecryptReversiblePassword() throws Exception {
        String text = "YWRtaW46MTM5MTc2MTA4MTY4MTphZG1pbjoxMzkxNzYxMDgxNjgxOnBjZH5HNTc2NF9fNDM2Yzg3ZmYwMjk3NGRlNDllZDk4NDBiYzMxMmNlMDA";
        final String out = PasswordHandler.encryptReversiblePassword(text);
        assertNotNull(out);
        System.out.println(out);
    }


    @Test
    public void encryptPassword() {

        String username = "admin";
        String password = "Zsfz@2013";

        final String pwd = PasswordHandler.encryptPassword(password, username);
        assertNotNull(pwd);
//        System.out.println(pwd);
    }

}