package com.andaily.zhishifenzi.infrastructure;

import org.apache.commons.lang.math.RandomUtils;
import org.testng.annotations.Test;

import java.util.*;

import static org.testng.AssertJUnit.assertNotNull;

/**
 * 2022/10/18 19:55
 *
 * @author Shengzhao Li
 */
public class RandomDateTest {


    @Test
    public void random() {

        //天数的范围
        int round = 30;
        //需要生成的数量
        int iter = 5;

        List<Date> dateList = new ArrayList<>(iter);
        Date now = DateUtils.now();
        Set<Integer> existRandoms = new HashSet<>();
        for (int i = 0; i < iter; i++) {
            int random;
            do {
                random = RandomUtils.nextInt(round);
                existRandoms.add(random);
            } while (!existRandoms.contains(random));

            Date date = org.apache.commons.lang.time.DateUtils.addDays(now, -random);
            assertNotNull(date);
            dateList.add(date);
        }

        Collections.sort(dateList);
        for (Date date : dateList) {
            String dateText = DateUtils.toDateText(date);
            assertNotNull(dateText);
            System.out.println(dateText + "  " + RandomUtils.nextInt(20));
        }

    }
}
